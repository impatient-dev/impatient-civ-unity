﻿using imp.civ.unity;
using imp.civ.wg;
using imp.civ.world;
using UnityEngine;


public class MainUScript : MonoBehaviour {
	private UCamController CamController;
	private UTileHighlight SelectionHighlight;
	private USidebar Sidebar;
	private World W;

	void Start() {
		Debug.Log("Starting impatient-civ.");
		Application.logMessageReceived += LogCallback;
		Time.timeScale = 1; //getting set to 0 for some reason

		var wg = new WGSettings(GridTypes.Square, 80, 40) {Seed = 123, SpawnPortals = 1};

		W = WorldGenerator.Generate(wg);
		
		var r = new AppRes();
		SelectionHighlight = new UTileHighlight(r, W);
		Sidebar = new USidebar(W, r);
		CamController = new UCamController(Camera.main);
		new UWorldRenderer(W, r);

		CamController.PanTo(W, W.T.Require(W.X1 / 2, W.Y1 / 2));
	}


	void Update() {
		CamController.Update();

		var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		var pos = W.G.TileContaining(mousePos.x, mousePos.y);
		var tile = W.T.Opt(pos);
		SelectionHighlight.Show(tile);
		Sidebar.ShowTileOpt(tile);
	}



	private void LogCallback(string condition, string stackTrace, LogType type) {
		if(type == LogType.Exception || type == LogType.Error || type == LogType.Assert) {
			Debug.LogWarning("Error encountered; shutting down.");
			Time.timeScale = 0;
			this.enabled = false;
			Application.Quit();
		}
	}
}
