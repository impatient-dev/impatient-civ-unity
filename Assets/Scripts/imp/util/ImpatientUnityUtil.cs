﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TextPro = TMPro.TextMeshProUGUI;

using static System.Runtime.CompilerServices.MethodImplOptions;
using Object = UnityEngine.Object;


namespace imp.util {

	/**Utilities for working with Unity.
	 * AR means AddRequired, and returns the created component. W means With, and returns this.
	 * Generally a component has either an AR or a W function, not both - whichever is the more typical usage.*/
	public static class ImpatientUnityUtil {
		
		
		
		// MATH & VECTORS

		[MethodImpl(AggressiveInlining)] public static Vector2 V2(float x, float y) => new Vector2(x, y);
		[MethodImpl(AggressiveInlining)] public static Vector2Int V2I(int x, int y) => new Vector2Int(x, y);
		[MethodImpl(AggressiveInlining)] public static Vector3 V3(float x, float y, float z = 0) => new Vector3(x, y, z);
		[MethodImpl(AggressiveInlining)] public static Vector3 V3xz(float x, float z) => new Vector3(x, 0, z);

		/**Adds a Z component.*/
		[MethodImpl(AggressiveInlining)] public static Vector3 Augment(this Vector2 vec, float z = 0) => new Vector3(vec.x, vec.y, z);
		/**Adds a Y component, and shifts the current Y to Z.*/
		[MethodImpl(AggressiveInlining)] public static Vector3 AugShift(this Vector2 vec, float y = 0) => new Vector3(vec.x, y, vec.y);
		
		/**Removes the Z component.*/
		[MethodImpl(AggressiveInlining)] public static Vector2 xy(this Vector3 vec) => new Vector2(vec.x, vec.y);
		/**Removes the Y component, turning Z into Y.*/
		[MethodImpl(AggressiveInlining)] public static Vector2 xz(this Vector3 vec) => new Vector2(vec.x, vec.z);

		/**Takes the absolute value of each component.*/
		[MethodImpl(AggressiveInlining)] public static Vector2Int Abs(this Vector2Int vec) => new Vector2Int(Math.Abs(vec.x), Math.Abs(vec.y));

		[MethodImpl(AggressiveInlining)] public static float Dot(this Vector3 a, Vector3 b) => Vector3.Dot(a, b);

		/**Rotates 90 degrees counter-clockwise.*/
		public static Vector2 RotL(this Vector2 vec) => new Vector2(-vec.y, vec.x);
		/**Rotates 90 degrees clockwise.*/
		public static Vector2 RotR(this Vector2 vec) => new Vector2(vec.y, -vec.x);
		/**Rotates 90 degrees counter-clockwise (left) or clockwise (right).*/
		public static Vector2 RotL(this Vector2 vec, bool left) => left ? vec.RotL() : vec.RotR();

		/**Returns true if b is to the right of a and false if b is to the left.
		 * The result is undefined for vectors that are identical or opposite in orientation.*/
		public static bool VecIsRight(Vector2 a, Vector2 b) => Vector3.Cross(a.Augment(), b.Augment()).z > 0;



		// UNITY GAME OBJECTS

		/**Creates a child GameObject with a specific name.*/
		public static GameObject AddChild(this GameObject parent, string childName) {
			var child = new GameObject(childName);
			child.transform.parent = parent.transform;
			return child;
		}

		/**Creates a child GameObject if the parent is present, or a GameObject with no parent if the parent is null.*/
		public static GameObject ChildGameObject([CanBeNull] GameObject parent, string name) => parent?.AddChild(name) ?? new GameObject(name);

		/**Finds a component. Throws an exception if the component is not found, instead of returning null.*/
		public static T Require<T>(this GameObject obj) where T : Component => obj.GetComponent<T>() ?? throw new Exception($"Component not found on object {obj.name}");

		public static void Destroy(this GameObject obj) => Object.Destroy(obj);

		/**Sets the local position, relative to any parent transform. Returns this.*/
		public static GameObject LocalPos(this GameObject obj, Vector3 pos) {
			obj.transform.localPosition = pos;
			return obj;
		}
		/**Sets the local rotation, relative to any parent transform. Returns this.*/
		public static GameObject LocalRot(this GameObject obj, Quaternion rot) {
			obj.transform.localRotation = rot;
			return obj;
		}
		/**Sets the local position and rotation, relative to any parent transform. Returns this.*/
		public static GameObject LocalTransform(this GameObject obj, Vector3 pos, Quaternion rot) {
			var transform = obj.transform;
			transform.position = pos;
			transform.rotation = rot;
			return obj;
		}
		
		/**Sets the transform's local position to (0,0,0) and its rotation to identity.
		 * Does not change whether or not the transform has a parent.*/
		public static void Reset(this Transform transform) {
			transform.localPosition = Vector3.zero;
			transform.rotation = Quaternion.identity;
		}

		/**Sets the transform's position to (0,0,0), its rotation to identity, and its parent to nothing.*/
		public static void DetachAndReset(this Transform transform) {
			transform.parent = null;
			transform.localPosition = Vector3.zero;
			transform.rotation = Quaternion.identity;
		}

		
		
		// ADD COMPONENTS
		//alphabetical by component

		/**Adds and returns a component. The component is required - an exception will be thrown if the component is not added.*/
		public static T AR<T>(this GameObject obj) where T : Component => obj.AddComponent<T>() ?? throw new Exception("Component was  not added to game object " + obj.name);

		/**Adds an Image to serve as a background.*/
		public static GameObject WBackground(this GameObject obj, Color color) {
			obj.AR<Image>().color = color;
			return obj;
		}
		
		/**Adds a Canvas. Also adds a GraphicRaycaster, because otherwise the canvas doesn't work as a UI.*/
		public static GameObject WCanvas(this GameObject obj, RenderMode renderMode = RenderMode.ScreenSpaceOverlay) {
			obj.AR<Canvas>().renderMode = renderMode;
			obj.AR<GraphicRaycaster>(); 
			return obj;
		}

		/**Adds and returns a HorizontalLayoutGroup.*/
		public static HorizontalLayoutGroup ARHorizontal(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) =>
			obj.AR<HorizontalLayoutGroup>().Set(childAlign);
		/**Adds a HorizontalLayoutGroup.*/
		public static GameObject WHorizontal(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter, int spacing = 0) {
			var row = obj.AR<HorizontalLayoutGroup>().Set(childAlign);
			row.spacing = spacing;
			return obj;
		}
		/**Adds a center-left-aligned HorizontalLayoutGroup.*/
		public static GameObject WHorizontalL(this GameObject obj, int spacing = 0) => obj.WHorizontal(TextAnchor.MiddleLeft, spacing);

		/**Creates a LineRenderer with all information necessary to display some lines.*/
		public static LineRenderer ARLine(this GameObject obj, float width, Material material, params Vector3[] positions) {
			var ret = obj.AR<LineRenderer>();
			ret.useWorldSpace = false;
			ret.startWidth = ret.endWidth = width;
			ret.material = material;
			ret.SetPositions(positions);
			return ret;
		}

		/**Creates a LineRenderer with the position count specified, but not any of the actual positions - you have to specify them later.*/
		public static LineRenderer ARLine(this GameObject obj, float width, Material material, int positionCount) {
			var ret = obj.AR<LineRenderer>();
			ret.useWorldSpace = false;
			ret.startWidth = ret.endWidth = width;
			ret.material = material;
			ret.positionCount = positionCount;
			return ret;
		}

		public static MeshRenderer ARMesh(this GameObject obj, Mesh mesh, [CanBeNull] Material material = null) {
			obj.AR<MeshFilter>().mesh = mesh;
			var ret = obj.AR<MeshRenderer>();
			if(material != null)
				ret.material = material;
			return ret;
		}

		public static SpriteRenderer ARSprite(this GameObject obj, Sprite sprite, Material material, [CanBeNull] MaterialPropertyBlock props) {
			var ret = obj.AR<SpriteRenderer>();
			ret.sprite = sprite;
			ret.material = material;
			ret.SetPropertyBlock(props);
			return ret;
		}

		/**Creates text that appears in the world (not in a Canvas). ("G" = general)
		 * fontSize should be the approximate desired height in world coordinates - we'll multiply this by 10 and send it to TextMeshPro.*/
		public static TextMeshPro ARGText(this GameObject obj, TMP_FontAsset font, Color color, float fontSize, string text = "",
			TextAlignmentOptions alignment = TextAlignmentOptions.Center, FontStyles style = FontStyles.Normal) {
			var ret = obj.AR<TextMeshPro>();
			ret.font = font;
			ret.color = color;
			ret.fontSize = fontSize * 10;
			ret.text = text;
			ret.alignment = alignment;
			ret.fontStyle = style;
			return ret;
		}
		
		/**Creates text that appears in a Canvas (not in the world).
		 * You need to supply a material because by default, TextMeshPro text can appear behind world objects (even in an overlay canvas - WTF).*/
		public static TextPro ARCText(this GameObject obj, Material material) {
			var ret = obj.AR<TextMeshProUGUI>();
			ret.material = material;
			return ret;
		}

		/**Adds and returns a VerticalLayoutGroup.*/
		public static VerticalLayoutGroup ARVertical(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) =>
			obj.AR<VerticalLayoutGroup>().Set(childAlign);
		/**Adds a VerticalLayoutGroup.*/
		public static GameObject WVertical(this GameObject obj, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<VerticalLayoutGroup>().Set(childAlign);
			return obj;
		}
		/**Adds a VerticalLayoutGroup with padding.*/
		public static GameObject WVertical(this GameObject obj, RectOffset padding, TextAnchor childAlign = TextAnchor.MiddleCenter) {
			obj.AR<VerticalLayoutGroup>().Set(childAlign).padding = padding;
			return obj;
		}
		
		
		
		// CUSTOMIZE COMPONENTS
		//alphabetical by component
		
		/**Convenience function to set everything we normally care about in a layout component, with sensible defaults.*/
		public static T Set<T>(this T layout, TextAnchor childAlign = TextAnchor.MiddleCenter, bool childControlsWidth = true, bool childControlsHeight = true,
			bool childForceExpandWidth = false, bool childForceExpandHeight = false) where T : HorizontalOrVerticalLayoutGroup {
			layout.childAlignment = childAlign;
			layout.childControlWidth = childControlsWidth;
			layout.childControlHeight = childControlsHeight;
			layout.childForceExpandWidth = childForceExpandWidth;
			layout.childForceExpandHeight = childForceExpandHeight;
			return layout;
		}

		public static LineRenderer Positions(this LineRenderer lr, params Vector3[] positions) {
			lr.SetPositions(positions);
			return lr;
		}
		
		
		
		// RESOURCES
		
		/**Creates a gray color, where red/green/blue are the same.*/
		public static Color Gray(float rgb, float a = 1) => new Color(rgb, rgb, rgb, a);

		/**Creates a color from a hexidecimal number. The alpha is always the max value.*/
		public static Color32 HexColor(int hex) {
			var r = (hex & 0xff0000) >> 16;
			var g = (hex & 0xff00) >> 8;
			var b = hex & 0xff;
			return new Color32((byte)r, (byte)g, (byte)b, byte.MaxValue);
		}
		
		/**Throws an exception if the resource isn't found.*/
		public static T LoadResource<T>(string path) where T : UnityEngine.Object => Resources.Load<T>(path) ?? throw new Exception("Resource " + path + " not found.");
		
		/**Creates a TextMeshPro font.*/
		public static TMP_FontAsset Pro(this Font font) => TMP_FontAsset.CreateFontAsset(font);



		// UI

		/**Figures out whether the user is hovering over a canvas/UI element.
		 * Returns false if there is no current EventSystem, or if the current EventSystem says the mouse is not hovering over it.*/
		public static bool PointingAtUi() {
			var sys = EventSystem.current;
			if (sys == null)
				return false;
			return sys.IsPointerOverGameObject();
		}
	}

}