﻿using imp.civ.ui;
using imp.civ.unity;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.civ {

	public static class CivStyle {
		public static readonly Color Background = Color.black;
		/**UI panel background color.*/
		public static readonly Color UiBack = Gray(.3f);
		/**UI text color.*/
		public static readonly Color UiFront = Gray(.8f);
		
		public static readonly RectOffset Pad = new RectOffset(5, 5, 5, 5);

		/**A header.*/
		public static CText CivH2(this CText text, AppRes r) => text.Font(r.Font.Bold, 20).Color(UiFront);
		/**Body text.*/
		public static CText CivBody(this CText text, AppRes r) => text.Font(r.Font.Main, 18).Color(UiFront);
		/**Small label text.*/
		public static CText CivSmallLabel(this CText text, AppRes r) => text.Font(r.Font.Main, 12).Color(UiFront);
	}
}