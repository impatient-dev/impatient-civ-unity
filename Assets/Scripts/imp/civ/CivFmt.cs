﻿namespace imp.civ {

	/**Formats numbers.*/
	public static class CivFmt {
		private const float MIN_DEPTH = 0.001f; // 1 mm
		
		
		/**Turns the hash code into a hexadecimal string.*/
		public static string HexHash(this object obj) => obj.GetHashCode().ToString("X");
		
		/**Displays the altitude of the ground (in meters).*/
		public static string FmtLandAlt(this float alt, bool includeUnit = true) {
			var ret = ((int)alt).ToString();
			if(includeUnit) ret += " m";
			return ret;
		}

		public static string FmtLiquidDepth(this float depth, bool includeUnit = true) {
			if(depth < 0)
				return '-' + (-depth).FmtLiquidDepth(includeUnit);
			string ret;
			if(depth < MIN_DEPTH)
				ret = MIN_DEPTH.ToString();
			else if(depth < 0.2f)
				ret = depth.ToString("F3");
			else if(depth < 1)
				ret = depth.ToString("F2");
			else if(depth < 10)
				ret = depth.ToString("F1");
			else
				ret = ((int)depth).ToString();
			if(includeUnit) ret += " m";
			return ret;
		}
	}
}