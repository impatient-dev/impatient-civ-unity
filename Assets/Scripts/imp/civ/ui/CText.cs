using imp.civ.unity;
using imp.util;
using TMPro;
using UnityEngine;

namespace imp.civ.ui {
	/**Canvas (as opposed to 3D world) text, implemented using TextMeshPro. Supports method chaining.*/
	public readonly struct CText {
		public readonly TextMeshProUGUI Pro;
		
		public CText(TextMeshProUGUI pro) {
			Pro = pro;
		}

		public CText Centered() {
			Pro.alignment = TextAlignmentOptions.Center;
			return this;
		}
		public CText Left() {
			Pro.alignment = TextAlignmentOptions.Left;
			return this;
		}
		public CText Right() {
			Pro.alignment = TextAlignmentOptions.Right;
			return this;
		}

		public CText Font(TMP_FontAsset font, float size) {
			Pro.font = font;
			Pro.fontSize = size;
			return this;
		}
		public CText Color(Color color) {
			Pro.color = color;
			return this;
		}

		public CText Text(string str) {
			Pro.text = str;
			return this;
		}
		public string text => Pro.text;
	}




	public static class CTextExtensions {
		public static CText ARCText(this GameObject obj, AppRes r) {
			return new CText(obj.ARCText(r.Mat.Color.Mat));
		}
	}
}