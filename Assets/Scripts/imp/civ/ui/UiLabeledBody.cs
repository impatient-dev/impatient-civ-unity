using imp.civ.unity;
using imp.util;
using UnityEngine;
using UnityEngine.UI;

namespace imp.civ.ui {

	/**A row containing body text plus a small label describing the body.*/
	public class UiLabeledBody : UWrapperS {
		private readonly CText _lbl, _txt;
		
		public UiLabeledBody(GameObject parent, AppRes r, string name, string label = "", string text = "") : base(parent, name) {
			Obj.WHorizontalL(UIS.hBody);
			_lbl = Obj.AddChild("lbl").ARCText(r).CivSmallLabel(r).Text(label);
			_txt = Obj.AddChild("txt").ARCText(r).CivBody(r).Text(text);
		}

		public string label {
			get => _lbl.text;
			set => _lbl.Text(value);
		}

		public string text {
			get => _txt.text;
			set => _txt.Text(value);
		}

		public UiLabeledBody With(TextAnchor childAlign) {
			Obj.Require<HorizontalLayoutGroup>().childAlignment = childAlign;
			return this;
		}

		public void Show(string text) {
			this.text = text;
			Show();
		}
	}

}