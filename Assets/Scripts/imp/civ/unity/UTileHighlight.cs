﻿using System;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.civ.unity {

	/**Draws lines around the currently selected tile. Inactive until you tell us what tile to highlight.*/
	public class UTileHighlight : UWrapperS {
		private readonly World W;
		private USideMesh[] _sides = Array.Empty<USideMesh>();
		[CanBeNull] private WTile _tile = null;
		private float _lineWidth = 0.1f;
		private Material _material;

		public UTileHighlight(AppRes r, World w) : base(new GameObject("Tile Highlight")) {
			W = w;
			_material = r.Mat.UiSel;
			Obj.LocalPos(new Vector3(0, 0, RD.SIDE));
		}

		[CanBeNull] public WTile Tile => _tile;

		/**Destroys the current meshes, and generates new ones if we have a tile.*/
		private void RegenSides() {
			foreach(var u in _sides)
				u.Destroy();
			if(_tile == null)
				_sides = Array.Empty<USideMesh>();
			else
				_sides = _tile.ListSides(W).Map(side => new USideMesh(W, side, _lineWidth, _material, Obj));
		}

		public void Show([CanBeNull] WTile tile) {
			if(_tile == tile)
				return;
			_tile = tile;
			RegenSides();
		}

		public void Show([CanBeNull] WTile tile, Material material, float lineWidth) {
			if(_tile == tile && _material == material && _lineWidth == lineWidth)
				return;
			_tile = tile;
			_material = material;
			_lineWidth = lineWidth;
			RegenSides();
		}
	}

}