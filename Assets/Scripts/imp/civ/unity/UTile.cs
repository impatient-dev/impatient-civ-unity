using System.Collections.Generic;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.civ.unity {
	/**Displays a mesh covering a tile, with the specified material and (optionally) color.*/
	public class UTile : UWrapperS {
		private readonly MeshRenderer Renderer;
		private readonly World W;
		private WTile _tile;
		private Material _material;
		private Color _color;

		public UTile(World w, WTile tile, Material material, [CanBeNull] MaterialPropertyBlock properties, GameObject parent) : base(parent, tile.Id.ToString()) {
			W = w;
			Obj.LocalPos(tile.Center(w).Augment(RD.TILE));
			Renderer = Obj.ARMesh(MyMeshes.Get(w.G), material);
			Renderer.SetPropertyBlock(properties);
			_tile = tile;
		}

		/**Constructor variant for when you don't want to set the material color.*/
		public UTile(World w, WTile tile, Material material, GameObject parent) : this(w, tile, material, null, parent) {}
		
		public WTile Tile => _tile;

		public void Show(WTile tile, Material mat, [CanBeNull] MaterialPropertyBlock properties = null) {
			if(_tile != tile) {
				Obj.LocalPos(tile.Center(W));
				_tile = tile;
			}
			Show(mat, properties);
		}

		public void Show(Material mat, [CanBeNull] MaterialPropertyBlock properties = null) {
			Renderer.material = mat;
			_material = mat;
			Show(properties);
		}

		public void Show([CanBeNull] MaterialPropertyBlock properties) {
			Renderer.SetPropertyBlock(properties);
			Show();
		}



	private static class MyMeshes {
		private static readonly Dictionary<GridType, Mesh> ByGridType = new Dictionary<GridType, Mesh>();

		/**Returns a mesh that covers any tile in the provided grid. The mesh is centered at the origin, not any particular tile.*/
		public static Mesh Get(GridType g) => ByGridType.GetOrCreate(g, () => {
			var tile = new TID(0, 0);
			var center = g.Center(tile);
			var vertices = g.VerticesAround(tile).Map(v => (g.VertexPos(v) - center).Augment());
			var mesh = new Mesh();
			mesh.vertices = vertices;
			mesh.triangles = g.FillTriangleIndices;
			return mesh;
		});
	}
	}
}