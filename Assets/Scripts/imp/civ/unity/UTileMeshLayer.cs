using System.Collections.Generic;
using imp.civ.util;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.civ.unity {
	/**A set of tile meshes, 1 per tile. You can choose what material to use for each tile, and can set the material properties.
	 * The chooser function determines what's shown for each tile. If the chooser returns null, that tile mesh is not shown.
	 * If the chooser is null, no tiles are shown.*/
	public class UTileMeshLayer : UWrapper {
		private readonly World W;
		private readonly Dictionary<TID, UTile> Display = new Dictionary<TID, UTile>();
		private TileAABB _region;
		[CanBeNull] private TileMatChooser _chooser;

		public UTileMeshLayer(World w, TileAABB region, [CanBeNull] TileMatChooser chooser = null) : base(new GameObject("Tile Meshes")) {
			W = w;
			_region = region;
			_chooser = chooser;
			Invalidate();
		}

		/**Decides what material to display for each tile. If this is null, no tiles are shown.*/
		[CanBeNull] public TileMatChooser Chooser {
			get => _chooser;
			set {
				_chooser = value;
				Invalidate();
			}
		}

		/**Makes this object recheck what material should be shown for every currently-visible tile.*/
		public void Invalidate() {
			if(_chooser == null) {
				foreach(var mesh in Display.Values)
					mesh.Destroy();
				Display.Clear();
			} else {
				_region.ForEachTile(W, tile => {
					var current = Display.Opt(tile.Id);
					var desired = _chooser.Choose(tile);
					Apply(tile, current, desired);
				});
			}
		}

		/**Rechecks what material should be shown for a particular tile.*/
		public void Invalidate(WTile tile) {
			if(!_region.Contains(tile.Id) || _chooser == null)
				return;
			var current = Display.Opt(tile.Id);
			var desired = _chooser.Choose(tile);
			Apply(tile, current, desired);
		}

		/**Given the current and desired display for a tile, makes any necessary change.*/
		private void Apply(WTile tile, [CanBeNull] UTile current, MatChoice choice) {
			if(choice.Material == null) { // hide any existing mesh
				if(current != null) {
					current.Destroy();
					Display.Remove(tile.Id);
				}
			} else { // show mesh, create if necessary
				if(current == null) {
					current = new UTile(W, tile, choice.Material, choice.Properties, Obj);
					Display[tile.Id] = current;
				} else {
					current.Show(choice.Material, choice.Properties);
				}
			}
		}
	}


	public interface TileMatChooser {
		/**Chooses what material to apply to a tile mesh. If this returns null, the mesh will not be shown for that particular tile.*/
		MatChoice Choose(WTile tile);
	}


	/**A choice of material, and optionally of properties that should be set on the material.*/
	public readonly struct MatChoice {
		[CanBeNull] public readonly Material Material;
		[CanBeNull] public readonly MaterialPropertyBlock Properties;

		public MatChoice([CanBeNull] Material mat = null, [CanBeNull] MaterialPropertyBlock properties = null) {
			Material = mat;
			Properties = properties;
		}
	}
}