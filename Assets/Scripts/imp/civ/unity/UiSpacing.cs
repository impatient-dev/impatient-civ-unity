namespace imp.civ.unity {
	/**UI spacing - constants for how large spaces in the UI should be.*/
	public static class UIS {
		/**Horizontal separation between body and other text.*/
		public static readonly int hBody = 12;
	}
}