﻿using imp.civ.unity.layer;
using imp.civ.util;
using imp.civ.world;
using imp.civ.world.land;
using imp.util;
using UnityEngine;

namespace imp.civ.unity {

	/**Renders a world - tiles, edges, etc.
	 * This class is not involved in rendering the Canvas UI (sidebar, etc).*/
	public class UWorldRenderer {
		private readonly World W;
		private readonly AppRes R;
		private readonly UTileMeshLayer TileMeshes;

		public UWorldRenderer(World w, AppRes r){
			W = w;
			R = r;
			TileMeshes = new UTileMeshLayer(W, TileAABB.World(W), new TileTerrainLayer(W, R));
			var props = R.Mat.Color.Block(Color.green);
			
			for(int x = 0; x <= W.X1; x++)
				for(int y = 0; y <= W.Y1; y++) {
					var tile = W.T.Require(new TID(x, y));
					if(tile.AnyPlot(p => p.Use is SpawnPortal))
						new USvg(W.G.Center(tile.Id).Augment(RD.TILE_ICON), R.Svg.Portal, R.Mat.Color.Mat, props);
				}
		}
	}

}