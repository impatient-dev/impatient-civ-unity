﻿namespace imp.civ.unity {

	/**Render Depths: specifies the Z coordinate different things should be rendered at. Things at lower Z coordinates cover things at higher coordinates.
	 * All coordinates are integers, so you can add/subtract 0.1 etc. to something if it's not common enough to get its own entry here.*/
	public static class RD {
		public static readonly float TILE = 4f;
		public static readonly float EDGE = 3f;
		public static readonly float SIDE = 2f;
		public static readonly float VERTEX = 1f;
		public static readonly float TILE_ICON = -1f;
		public static readonly float SELECTION = -2f;
		public static readonly float CAMERA = -10f;
	}

}