using imp.civ.world;
using imp.util;
using UnityEngine;

namespace imp.civ.unity {

	/**Moves the camera when WSAD or the arrow keys are pressed; zooms the camera when the mouse wheel scrolls.*/
	public class UCamController {
		/**pixels per cellish*/
		private static readonly float ZOOM_MIN = 8, ZOOM_MAX = 1024;
		/**Multiply/divide the zoom by this each scroll - must be greater than 1.*/
		private static readonly float ZOOM_SPEED = 1.2f;
		/**pixels per second*/
		private static readonly float PAN_SPEED = 1000;

		private readonly Camera Camera;
		/**pixels per cellish*/
		private float Zoom = 150;

		public UCamController(Camera camera) {
			Camera = camera;
			Camera.orthographicSize = CamOrthoSize();
		}

		public void Update() {
			// zoom
			// TODO use the value of scroll instead of just the sign
			var scroll = Input.mouseScrollDelta.y;
			if(scroll != 0) {
				if (scroll > 0)
					Zoom = Mathf.Min(ZOOM_MAX, Zoom * ZOOM_SPEED); // zoom in
				else
					Zoom = Mathf.Max(ZOOM_MIN, Zoom / ZOOM_SPEED);
				Camera.orthographicSize = CamOrthoSize();
			}
		
			//pan
			var x = Input.GetAxisRaw("Horizontal") * PAN_SPEED * Time.deltaTime / Zoom;
			var y = Input.GetAxisRaw("Vertical") * PAN_SPEED * Time.deltaTime / Zoom;
			Camera.transform.Translate(x, y, 0);
		}


		public void PanTo(Vector2 pos) {
			Camera.transform.position = pos.Augment(RD.CAMERA);
		}
		public void PanTo(World w, WTile tile) => PanTo(tile.Center(w));

		// TODO only change on screen resize
		private float CamOrthoSize() => Screen.height / Zoom / 2;
	}
}