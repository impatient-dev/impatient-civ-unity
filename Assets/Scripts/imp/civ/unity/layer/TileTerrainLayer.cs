using imp.civ.world;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.civ.unity.layer {

	public class TileTerrainLayer : TileMatChooser {
		private readonly World W;
		private readonly AppRes R;
		private readonly MaterialPropertyBlock _ocean, _sea, _flooded, _barren;


		public TileTerrainLayer(World w, AppRes r) {
			W = w;
			R = r;
			_ocean = R.Mat.Color.Block(HexColor(0x1A3B76));
			_sea = R.Mat.Color.Block(HexColor(0x266DA6));
			_flooded = R.Mat.Color.Block(HexColor(0x39B99A));
			_barren = R.Mat.Color.Block(HexColor(0x5F6507));
		}

		public MatChoice Choose(WTile tile) {
			var block = ChooseBlock(tile);
			return new MatChoice(R.Mat.Color.Mat, block);
		}
		
		private MaterialPropertyBlock ChooseBlock(WTile tile) {
			if(tile.LiquidDepth > 100) return _ocean;
			if (tile.LiquidDepth > 5 || tile.LiquidAlt > tile.AltMax) return _sea;
			if (tile.LiquidDepth > 0) return _flooded;
			return _barren;
		}

	}
}