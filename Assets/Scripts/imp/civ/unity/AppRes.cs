using TMPro;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.civ.unity {

	/**Resources used by rendering/GUI. Don't modify any of these at runtime.*/
	public class AppRes {
		public readonly AppFonts Font = new AppFonts();
		public readonly AppMaterials Mat = new AppMaterials();
		public readonly AppSvg Svg = new AppSvg();
	}


	//for reference, the default can be accessed via Resources.GetBuiltinResource<Font>("Arial.ttf")
	public class AppFonts {
		public readonly TMP_FontAsset Main = LoadResource<TMP_FontAsset>("Font-TMP/FreeSans");
		public readonly TMP_FontAsset Bold = LoadResource<TMP_FontAsset>("Font-TMP/FreeSansBold");
	}

	public class AppMaterials {
		/**A plain material that just renders a color. Don't modify this material directly (e.g. by changing the color); clone the material first, or use MaterialPropertyBlock.*/
		public readonly ColorMat Color = new ColorMat("Materials/Color");
	
		/**selection*/
		public readonly Material UiSel = LoadResource<Material>("Materials/UiSel");
	}


	public class AppSvg {
		public readonly Sprite Portal = LoadResource<Sprite>("SVG/Portal");
	}


	/**A Material where the only property is _Color.*/
	public class ColorMat {
		public readonly Material Mat;
		/**The color property ID for the shader.*/
		public readonly int Prop;

		public ColorMat(string path) {
			Mat = LoadResource<Material>(path);
			Prop = Shader.PropertyToID("_Color");
		}

		/**Creates a MaterialPropertyBlock setting the color of this material.*/
		public MaterialPropertyBlock Block(Color color) {
			var ret = new MaterialPropertyBlock();
			ret.SetColor(Prop, color);
			return ret;
		}
	}
}