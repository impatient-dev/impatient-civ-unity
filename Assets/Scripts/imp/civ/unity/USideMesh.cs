using imp.civ.util;
using imp.civ.world;
using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.civ.unity {
	/**A mesh that covers a side of a tile. It's trapezoid-shaped to avoid overlap with any other side of this or any other tile.
	 * Currently, instances of this class cannot be reused to show different sides.*/
	public class USideMesh : UWrapperH {
		private readonly MeshRenderer MeshRenderer;
		public readonly WSide Side;

		public USideMesh(World w, WSide side, float width, Material material, GameObject parent) : base(parent, side.Id.ToString()) {
			MeshRenderer = Obj.ARMesh(MyMeshBuilder.MakeMesh(w.G, side.Id, width), material);
			Side = side;
		}



		private static class MyMeshBuilder {
		/**The sin of an angle in a right triangle, useful for calculating some positions in the mesh.
		 * The hypotenuse of the right triangle is either of the diagonal sides of the trapezoid-shaped mesh.
		 * The angle is between the hypotenuse and the edge.*/
			private static readonly PerGridType<float> Sins = new PerGridType<float>(g => Mathf.Sin(g.InternalAngleDeg / 2));


			public static Mesh MakeMesh(GridType g , SID sid, float width) {
				var (va, vb) = g.VerticesOfEdge(sid.Edge);
				var posA = g.VertexPos(va);
				var posB = g.VertexPos(vb);
				var center = g.Center(sid.Tile);
				var diagonalDistance = width * Sins.Req(g);
				var innerA = Vector2.MoveTowards(posA, center, diagonalDistance);
				var innerB = Vector2.MoveTowards(posB, center, diagonalDistance);
				// whether the center is to the right of vector A->B
				var right = VecIsRight(posB - posA, center - posA);

				var mesh = new Mesh();
				mesh.vertices = new[] {
					posA.Augment(),
					posB.Augment(),
					innerA.Augment(),
					innerB.Augment(),
				};
				mesh.triangles = !right ? new[] { 0,1,2, 2,1,3 } : new[] { 2,3,0, 0,3,1 };
				return mesh;
			}
		}
	}
}