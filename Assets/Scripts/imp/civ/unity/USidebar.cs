using imp.civ.ui;
using imp.civ.world;
using imp.civ.world.land;
using imp.util;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.civ.unity {

	public class USidebar {
		private readonly World W;
		private readonly AppRes R;
		private readonly GameObject Root;
		[CanBeNull] private WTile _tile = null;
		private readonly CText Title;
		private readonly UiLabeledBody Alt, MaxAlt, WaterDepth, TotalPeople, Special;

		public USidebar([NotNull] World w, [NotNull] AppRes r){
			W = w;
			R = r;
		
			Root = new GameObject("World Canvas").WCanvas().WHorizontal(TextAnchor.MiddleRight);

			var panel = Root.AddChild("Sidebar").WBackground(CivStyle.UiBack).WVertical(CivStyle.Pad);

			var titleObj = panel.AddChild("Title").WHorizontal();
			Title = titleObj.ARCText(r).Centered().CivH2(r);

			Alt = new UiLabeledBody(panel, r, "alt", "alt");
			MaxAlt = new UiLabeledBody(panel, r, "max alt", "max");
			WaterDepth = new UiLabeledBody(panel, r, "depth", "water");
			TotalPeople = new UiLabeledBody(panel, r, "people", "people");
			Special = new UiLabeledBody(panel, r, "special", "special");

			for(var i = 1; i <= 10; i++)
				panel.AddChild($"test-{i}").WHorizontal().ARCText(r).CivBody(r).Text($"{i}");

			Root.SetActive(false);
		}

		/**Null hides this sidebar.*/
		public void ShowTileOpt([CanBeNull] WTile tile) {
			if (tile == _tile) {
				return;
			} else if (tile == null) {
				Root.SetActive(false);
			} else {
				Title.Text($"Tile ({tile.Id.X},{tile.Id.Y})");
				Alt.text = tile.AltBase.FmtLandAlt();
				MaxAlt.text = tile.AltMax.FmtLandAlt();
				WaterDepth.text = tile.LiquidDepth.FmtLiquidDepth();
				TotalPeople.text = "N/A";
				Special.text = (tile.AnyPlot(p => p.Use is SpawnPortal)) ? "Spawn Portal" : "—";
				Root.SetActive(true);
			}

			_tile = tile;
		}
	}
}
