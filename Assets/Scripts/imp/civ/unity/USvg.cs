using imp.util;
using JetBrains.Annotations;
using UnityEngine;

namespace imp.civ.unity {
	/**Displays a SVG icon in the world.*/
	public class USvg {
		private readonly GameObject U;

		public USvg(Vector3 center, Sprite sprite, Material material, [CanBeNull] MaterialPropertyBlock props = null) {
			U = new GameObject("Svg").LocalPos(center);
			U.ARSprite(sprite, material, props);
		}
	}
}