using System;
using imp.civ.world;
using imp.civ.world.land;
using imp.util;

namespace imp.civ.change {
	/**Makes changes to a world.
	 * Changes are centralized in this class so all changes can be consistent, complete, and appropriately validated.*/
	public static class ChWorld {
		/**Finds empty/natural land in a tile and transfers it to a new plot.*/
		public static LandPlot LandCreateFromEmpty(World w, WTile tile, LandUse use, Land amount) {
			var src = tile.Plots.Values.FirstOpt(p => p.Land >= amount && p.Use is EmptyLand)
				?? throw new Exception($"Not enough empty land in {tile} for {amount}");
			var dest = tile.AddPlot(w, amount, use);
			src.Land -= amount;
			if(src.Land == Land.ZERO)
				tile.RemovePlot(src);
			return dest;
		}
	}
}