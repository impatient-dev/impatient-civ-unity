using System;
using System.Collections.Generic;
using imp.civ.world;
using JetBrains.Annotations;
using static imp.util.ImpatientUtil;

namespace imp.civ.wg {

	/**A set of non-overlapping meta-regions which group up regions in the next lower level in the stack.
	 * The next lower level is either a set of tile regions or a set of smaller meta-regions.*/
	public class WGMetaLayer<T> : WGLayer<T> {
		private readonly HashSet<WGMetaRegion<T>> _regions = new HashSet<WGMetaRegion<T>>();
		private readonly Dictionary<WGRegion<T>, WGMetaRegion<T>> _map = new Dictionary<WGRegion<T>, WGMetaRegion<T>>();
		public readonly WGLayerStack<T> Stack;
		/**The index of this layer in the stack.*/
		public int Level { get; }

		public WGMetaLayer(WGLayerStack<T> stack, int level) {
			Level = level;
			Stack = stack;
		}

		public override string ToString() => $"MetaLayer(lvl {Level} #{this.HexHash()})";
		public IReadOnlyCollection<WGMetaRegion<T>> Regions => _regions;
		public IReadOnlyCollection<WGRegion<T>> GenericRegions => _regions;
		public bool Contains(WGRegion<T> subregion) => _map.ContainsKey(subregion);

		[CanBeNull] public WGMetaRegion<T> Opt(WGRegion<T> subregion) => _map.Opt(subregion);
		public WGMetaRegion<T> Require(WGRegion<T> subregion) => _map.Require(subregion);

		/**Assigns a subregion to a meta region.
		 * If reassign is false, the subregion must not be part of any region currently.*/
		public void Assign(WGRegion<T> subregion, WGMetaRegion<T> region, bool reassign = false) {
			if(!_regions.Contains(region))
				throw new Exception($"Region is not part of this layer: {region}.");
			CheckArg(subregion.GenericLayer.Level == Level - 1, () => $"Cannot add a level={subregion.GenericLayer.Level} region to a level={Level} meta-region.");
			var prev = _map.Opt(subregion);
			if(prev == region)
				return;
			if(prev != null && !reassign)
				throw new Exception($"Subregion {subregion} is already part of region {prev}.");
			_map[subregion] = region;
			region.OnAdd(subregion);
		}

		/**Creates a new meta-region and assigns the sub-region to it.
		 * If reassign is false, this subregion must not currently be part of any region.*/
		public WGMetaRegion<T> AssignToNew(WGRegion<T> subregion, T regionData, bool reassign = false) {
			CheckArg(subregion.GenericLayer.Level == Level - 1, () => $"Cannot add a level={subregion.GenericLayer.Level} region to a level={Level} meta-region.");
			var prev = _map.Opt(subregion);
			if(prev != null && !reassign)
				throw new Exception($"Sub-region {subregion} is already part of meta-region {prev}.");
			var region = new WGMetaRegion<T>(this, regionData);
			_regions.Add(region);
			_map[subregion] = region;
			region.OnAdd(subregion);
			return region;
		}

		/**Creates a new layer containing a different kind of data.*/
		public WGMetaLayer<R> ConvertData<R>(
			WGLayerStack<R> newStack,
			Func<WGMetaRegion<T>, R> dataConverter,
			Func<WGRegion<T>, WGRegion<R>> subregionMapper
		) {
			var newLayer = new WGMetaLayer<R>(newStack, Level);
			foreach(var region in _regions) {
				var newRegion = new WGMetaRegion<R>(newLayer, dataConverter.Invoke(region));
				newLayer._regions.Add(newRegion);
				foreach(var subregion in region.Subregions) {
					var newSubregion = subregionMapper.Invoke(subregion);
					newLayer._map[newSubregion] = newRegion;
					newRegion.OnAdd(newSubregion);
				}
			}
			return newLayer;
		}
	}


	/**A region composed of smaller regions (either tile regions or other meta-regions.*/
	public class WGMetaRegion<T> : WGRegion<T> {
		public readonly WGMetaLayer<T> Layer;
		private readonly List<WGRegion<T>> _subregions = new();
		public T Data { get; set; }

		public WGMetaRegion(WGMetaLayer<T> layer, T data) {
			Layer = layer;
			Data = data;
		}

		public override string ToString() => $"MetaRegion(lvl {Layer.Level} #{this.HexHash()})";
		public WGLayer<T> GenericLayer => Layer;
		public IReadOnlyCollection<WGRegion<T>> Subregions => _subregions;
		public bool Contains(WGRegion<T> subregion) => Layer.Opt(subregion) == this;

		public void OnAdd(WGRegion<T> subregion) {
			_subregions.Add(subregion);
		}
		public void Add(WGRegion<T> subregion, bool reassign = false) => Layer.Assign(subregion, this, reassign);

		public void ForEachTile(Action<TID> action) {
			foreach(var subregion in _subregions)
				subregion.ForEachTile(action);
		}

		/**Finds some tile that is indirectly contained in this region.*/
		public TID SomeTileReq() {
			WGMetaRegion<T> region = this;
			while(Layer.Level > 1)
				region = (WGMetaRegion<T>) region._subregions[0];
			WGTileRegion<T> tileRegion = (WGTileRegion<T>) region._subregions[0];
			return tileRegion.Tiles.FirstReq();
		}
	}

}