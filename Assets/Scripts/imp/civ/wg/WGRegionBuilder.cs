﻿using System;
using System.Collections.Generic;
using System.Linq;
using imp.civ.util;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;
using static imp.util.ImpatientUtil;

namespace imp.civ.wg {

	public static class WGRegionBuilder {
		/**Randomly divides the whole world into regions.*/
		public static WGTileLayer<T> DivideIntoRegions<T>(World w, int regions, Random rand, Func<T> dataCreator) {
			CheckArg(regions <= w.TileCount, () => $"Cannot fit {regions} regions into a {w.Width}x{w.Height} world.");

			// create regions
			var layer = new WGTileLayer<List<TID>>(); // the data is a set of frontier tiles that might border unclaimed tiles
			var frontierRegions = new List<WGTileRegion<List<TID>>>(regions);
			for(var c = 0; c < regions; c++) {
				var firstTile = UnclaimedTileOpt(w, layer, rand);
				if(firstTile == null)
					throw new Exception($"Unable to find a {c + 1}th unclaimed tile.");
				var region = layer.AssignToNew(firstTile, ListOf(firstTile.Id));
				frontierRegions.Add(region);
			}

			// assign all tiles to layers
			while(true) {
				UnclaimedFrontierTile(w, frontierRegions, layer, rand, out var tile, out var region);
				if(tile == null)
					break;
				layer.Assign(tile, region);
				region.Data.Add(tile.Id);
			}

			return layer.ConvertData(__ => dataCreator.Invoke());
		}

		/**Finds a random tile that's not part of any region in the layer.*/
		[CanBeNull] private static WTile UnclaimedTileOpt<T>(World w, WGTileLayer<T> layer, Random rand) {
			for(var c = 0; c < 100; c++) {
				var tile = rand.NextTile(w);
				if(layer.Opt(tile) == null)
					return tile;
			}

			return null;
		}

		/**Finds a region that can expand to an adjacent tile that's not yet part of a region, randomly.
		 * This function is based on frontiers: the region list is a list of frontier regions that might border unclaimed tiles,
		 * and the region data is a list of frontier tiles that might border unclaimed tiles.
		 * All frontier lists may be modified to remove things that are definitely not on the frontier anymore.
		 * This function will return null in both out params once the frontier is empty.*/
		private static void UnclaimedFrontierTile(
			World w,
			List<WGTileRegion<List<TID>>> frontierRegions,
			WGTileLayer<List<TID>> layer,
			Random rand,
			[CanBeNull] out WTile tile,
			[CanBeNull] out WGTileRegion<List<TID>> region
		) {
			while(frontierRegions.IsNotEmpty()) {
				var idx = rand.NextIdx(frontierRegions);
				var r = frontierRegions[idx];
				var t = UnclaimedNeighborTileUpdatingFrontier(w, r, layer, rand);
				if(t == null) {
					frontierRegions.RemoveAt(idx);
				} else {
					tile = t;
					region = r;
					return;
				}
			}

			tile = null;
			region = null;
		}

		/**Finds a random tile adjacent to this region but not yet part of any region.
		 * The region data is a list of frontier tiles (tiles in the region which might border unclaimed tiles), which will be updated by this function.*/
		[CanBeNull] private static WTile UnclaimedNeighborTileUpdatingFrontier(World w, WGTileRegion<List<TID>> region, WGTileLayer<List<TID>> layer, Random rand) {
			while(true) {
				if(region.Data.IsEmpty())
					return null;
				var idx = rand.NextIdx(region.Data);
				var tile = region.Data[idx];
				var neighbors = tile.In(w).ListTilesAround(w);
				var n = rand.NextIdx(neighbors);
				for(var dn = 0; dn < neighbors.Count; dn++) {
					var nn = (n + dn) % neighbors.Count;
					var neighbor = neighbors[nn];
					if(layer.Opt(neighbor) == null)
						return neighbor;
				}
				region.Data.RemoveAt(idx);
			}
		}



		/**Randomly divides the layer into meta-regions.*/
		public static WGMetaLayer<T> DivideIntoMetaRegions<T>(World w, int metaRegions, WGLayer<T> subLayer, WGLayerStack<T> stack, Random rand, Func<T> dataCreator) {
			var unclaimedSubRegions = new List<WGRegion<T>>(subLayer.GenericRegions);
			CheckArg(metaRegions <= unclaimedSubRegions.Count, () => $"Cannot divide {unclaimedSubRegions.Count} regions into {metaRegions} meta-regions.");
			var newLayer = new WGMetaLayer<T>(stack, subLayer.Level + 1);
			
			// create meta regions
			var frontierMetaRegions = new List<WGMetaRegion<T>>(metaRegions); // meta regions that might be able to expand
			// var frontierSubRegions = new Dictionary<WGMetaRegion<T>, WGRegion<T>>();
			for(var c = 0; c < metaRegions; c++) {
				var i = rand.NextIdx(unclaimedSubRegions);
				var region = newLayer.AssignToNew(unclaimedSubRegions[i], dataCreator.Invoke());
				frontierMetaRegions.Add(region);
				unclaimedSubRegions.RemoveAt(i);
			}
			
			// assign all sub-regions to meta-regions
			while(unclaimedSubRegions.IsNotEmpty()) {
				UnclaimedFrontierRegion(w, frontierMetaRegions, rand, out var subRegion, out var metaRegion);
				if(subRegion == null)
					throw new Exception($"{unclaimedSubRegions.Count} / {subLayer.GenericRegions.Count} sub-regions were not assigned to meta-regions.");
				newLayer.Assign(subRegion, metaRegion);
				unclaimedSubRegions.Remove(subRegion);
			}

			return newLayer;
		}

		/**Finds a meta-region that can expand to claim an adjacent sub-region that is not currently part of any meta-region, randomly.
		 * The lists of frontier regions will have entries removed if they are discovered to not be on the frontier anymore.
		 * This function will return null in both out params once the frontier is empty.*/
		private static void UnclaimedFrontierRegion<T>(
			World w,
			List<WGMetaRegion<T>> frontier,
			Random rand,
			[CanBeNull] out WGRegion<T> subRegion,
			[CanBeNull] out WGMetaRegion<T> metaRegion
		) {
			while(frontier.IsNotEmpty()) {
				var f = rand.NextIdx(frontier);
				var meta = frontier[f];
				var unclaimedNeighbors = FindUnclaimedNeighborSubRegions(w, meta);
				if(unclaimedNeighbors.IsEmpty()) {
					frontier.RemoveAt(f);
				} else {
					subRegion = rand.NextIn(unclaimedNeighbors.ToList());
					metaRegion = meta;
					return;
				}
			}

			subRegion = null;
			metaRegion = null;
		}

		/**Finds all regions that are adjacent to any sub-region in this meta-region but not part of any meta-region..*/
		private static HashSet<WGRegion<T>> FindUnclaimedNeighborSubRegions<T>(World w, WGMetaRegion<T> meta) {
			var ret = new HashSet<WGRegion<T>>();
			foreach(var subregion in meta.Subregions) {
				foreach(var neighbor in NeighborRegions(w, subregion, meta.Layer.Stack))
					if(!meta.Layer.Contains(neighbor))
						ret.Add(neighbor);
			}
			return ret;
		}

		/**Finds all regions in the same level that border this one.*/
		private static HashSet<WGRegion<T>> NeighborRegions<T>(World w, WGRegion<T> region, WGLayerStack<T> stack) {
			var ret = new HashSet<WGRegion<T>>();
			region.ForEachTile((regionTile) => {
				w.T.Require(regionTile).ForTilesAround(w, neighborTile => {
					var neighborRegion = stack.RegionOpt(neighborTile, region.GenericLayer.Level);
					if(neighborRegion != null && neighborRegion != region)
						ret.Add(neighborRegion);
				});
			});
			return ret;
		}
	}

}