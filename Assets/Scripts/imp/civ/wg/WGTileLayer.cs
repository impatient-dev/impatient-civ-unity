using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;

namespace imp.civ.wg {

	/**Divides the world into a set of non-overlapping regions that contain tiles.
	 * Each region has some piece of data (T) that you can modify (e.g. worldgen settings for that region).
	 * Initially contains no regions.*/
	public class WGTileLayer<T> : WGLayer<T> {
		private readonly HashSet<WGTileRegion<T>> _regions = new HashSet<WGTileRegion<T>>();
		private readonly Dictionary<TID, WGTileRegion<T>> _tiles = new Dictionary<TID, WGTileRegion<T>>();
		public int Level => 0;

		public override string ToString() => $"TileLayer(#{this.HexHash()})";
		public IReadOnlyCollection<WGTileRegion<T>> Regions => _regions;
		public IReadOnlyCollection<WGRegion<T>> GenericRegions => _regions;

		[CanBeNull] public WGTileRegion<T> Opt(TID tile) => _tiles.Opt(tile);
		[CanBeNull] [MethodImpl(MethodImplOptions.AggressiveInlining)] public WGTileRegion<T> Opt(WTile tile) => Opt(tile.Id);
		
		public WGTileRegion<T> Require(TID tile) => _tiles.Require(tile);
		[MethodImpl(MethodImplOptions.AggressiveInlining)] public WGTileRegion<T> Require(WTile tile) => Require(tile.Id);

		/**Assigns a tile to a region.
		 * If reassign is false, this tile must not currently be part of any region.*/
		public void Assign(TID tile, WGTileRegion<T> region, bool reassign = false) {
			if(!_regions.Contains(region))
				throw new Exception($"Region is not part of this layer: {region}");
			var prev = _tiles.Opt(tile);
			if(prev == region)
				return;
			if(prev != null && !reassign)
				throw new Exception($"Tile {tile} is already part of region {prev}.");
			_tiles[tile] = region;
			region.OnAdd(tile);
		}
		/**Assigns a tile to a region.
		 * If reassign is false, this tile must not currently be part of any region.*/
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Assign(WTile tile, WGTileRegion<T> region, bool reassign = false) => Assign(tile.Id, region, reassign);

		/**Creates a new region and assigns the tile to it.
		 * If reassign is false, this tile must not currently be part of any region.*/
		public WGTileRegion<T> AssignToNew(TID tile, T regionData, bool reassign = false) {
			var prev = _tiles.Opt(tile);
			if(prev != null && !reassign)
				throw new Exception($"Tile {tile} is already part of region {prev}.");
			var region = new WGTileRegion<T>(this, regionData);
			_regions.Add(region);
			_tiles[tile] = region;
			region.OnAdd(tile);
			return region;
		}
		/**Creates a new region and assigns the tile to it.
		 * If reassign is false, this tile must not currently be part of any region.*/
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public WGTileRegion<T> AssignToNew(WTile tile, T regionData, bool reassign = false) => AssignToNew(tile.Id, regionData, reassign);

		/**Creates a new layer containing a different kind of data.*/
		public WGTileLayer<R> ConvertData<R>(Func<WGTileRegion<T>, R> converter) {
			var ret = new WGTileLayer<R>();
			foreach(var r0 in _regions) {
				var r1 = r0.ConvertData(ret, converter.Invoke(r0));
				ret._regions.Add(r1);
				foreach(var tile in r1.Tiles)
					ret._tiles[tile] = r1;
			}
			return ret;
		}
	}


	/**A set of tiles, which should probably be contiguous, plus some associated data.
	 * Initially contains no tiles.*/
	public class WGTileRegion<T> : WGRegion<T> {
		public readonly WGTileLayer<T> Layer;
		private readonly List<TID> _tiles = new List<TID>();
		public T Data { get; set; }

		public WGTileRegion(WGTileLayer<T> layer, T data) {
			Layer = layer;
			Data = data;
		}

		public override string ToString() => $"TileRegion(#{this.HexHash()})";
		public WGLayer<T> GenericLayer => Layer;
		public IReadOnlyCollection<TID> Tiles => _tiles;

		public void OnAdd(TID tile) {
			_tiles.Add(tile);
		}
		public void Add(TID tile, bool reassign = false) => Layer.Assign(tile, this, reassign); 
		public void Add(WTile tile, bool reassign = false) => Add(tile.Id, reassign); 
		
		/**Converts to a region with a different kind of data.*/
		public WGTileRegion<R> ConvertData<R>(WGTileLayer<R> layer, R data) {
			var ret = new WGTileRegion<R>(layer, data);
			ret._tiles.AddRange(_tiles);
			return ret;
		}

		/**Combines our data with the data of any parent meta-region(s).
		 * The arguments to the combiner are (parentData, childData).*/
		public T CombineData(WGLayerStack<T> stack, Func<T, T, T> combiner) {
			var ret = Data;
			WGRegion<T> region = this;
			for(var layer = 1; layer < stack.Layers; layer++) {
				var parent = stack.Meta(layer).Require(region);
				ret = combiner(parent.Data, ret);
				region = parent;
			}
			return ret;
		}

		public void ForEachTile(Action<TID> action) {
			foreach(var tile in _tiles)
				action.Invoke(tile);
		}

		public TID SomeTileReq() => _tiles.FirstReq();
	}

}