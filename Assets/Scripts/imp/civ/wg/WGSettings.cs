﻿using System;
using imp.civ.world;

namespace imp.civ.wg {

	/**Settings for world generation.*/
	public class WGSettings {
		public GridType GridType;
		public int Width, Height;
		public int Seed = DateTime.Now.Second;
		/**In square meters.*/
		public float TileArea = 1_000_000;

		/**Number of spawn portals to place.*/
		public int SpawnPortals = 0;

		public WGTerrain Terrain = new WGTerrain();

		public WGSettings(GridType gridType, int width, int height) {
			GridType = gridType;
			Width = width;
			Height = height;
		}
	}


	/**Terrain generation settings.*/
	public class WGTerrain {
		/**The size of the regions used to randomly assign terrain.*/
		public float TilesPerRegion = 6;
		/**The size of the meta-regions used to randomly assign terrain.*/
		public float RegionsPerRegion = 5;

		/**False allows land to touch the edge of the world.*/
		public bool BorderIsOcean = true;
		/**Weights for different terrain types that determine how common they are in the world.*/
		public float Land = 0.5f, Ocean = 0.2f, Sea = 0.2f, Swamp = 0.1f;

		public float TotalWeight() => Land + Ocean + Sea + Swamp;
	}
}