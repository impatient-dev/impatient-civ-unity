using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using imp.civ.world;
using imp.util;
using JetBrains.Annotations;
using static System.Runtime.CompilerServices.MethodImplOptions;

namespace imp.civ.wg {

	/**A set of non-overlapping regions, containing either tiles or other regions (which eventually contain tiles).
	 * A layer cannot contain any empty regions.*/
	public interface WGLayer<T> {
		/**0 is the lowest layer, containing tiles.
		 * Layer N contains regions from layer N-1.*/
		int Level { get; }
		IReadOnlyCollection<WGRegion<T>> GenericRegions { get; }
	}

	/**A thing containing either tiles or other regions (which indirectly contain tiles).*/
	public interface WGRegion<T> {
		T Data { get; set; }
		WGLayer<T> GenericLayer { get; }
		/**Finds some tile that is indirectly contained in this region.*/
		TID SomeTileReq();
		/**Invokes the action for every tile directly or indirectly contained in this region.*/
		void ForEachTile(Action<TID> action);
	}

	
	/**A collection of related layers.
	 * A stack always contains a tile layer, and optionally contains 1 or more meta layers (which contain lower-level layers).*/
	public class WGLayerStack<T> {
		public readonly WGTileLayer<T> Tiles;
		private readonly List<WGMetaLayer<T>> _metas = new();

		public WGLayerStack(WGTileLayer<T> tiles) {
			Tiles = tiles;
		}
		
		public WGLayerStack(World w, int regions, Random rand, Func<T> dataCreator) : this(WGRegionBuilder.DivideIntoRegions(w, regions, rand, dataCreator)) {}

		public override string ToString() => $"LayerStack(#{this.HexHash()})";
		public int Layers => _metas.Count + 1;

		public WGLayer<T> TopLayer { get {
			if(_metas.IsNotEmpty())
				return _metas.LastReq();
			return Tiles;
		}}

		/**0 returns the tile layer; higher indexes return meta layers.*/
		public WGLayer<T> Layer(int idx) {
			if(idx == 0)
				return Tiles;
			return _metas[idx - 1];
		}
		/**Returns a meta layer. The index must be 1 or higher.*/
		public WGMetaLayer<T> Meta(int idx) => _metas[idx - 1];

		/**Divides the highest layer into the provided number of meta-regions, and adds those meta-regions as a new layer.*/
		public WGMetaLayer<T> AddNextMeta(World w, int regions, Random rand, Func<T> dataCreator) {
			WGLayer<T> subLayer = Tiles;
			if(_metas.IsNotEmpty())
				subLayer = _metas.LastReq();
			var meta = WGRegionBuilder.DivideIntoMetaRegions(w, regions, subLayer, this, rand, dataCreator);
			_metas.Add(meta);
			return meta;
		}

		/**Returns the region at the appropriate level that directly or indirectly contains this tile, if any.*/
		[CanBeNull] public WGRegion<T> RegionOpt(TID tile, int level) {
			WGRegion<T> region = Tiles.Opt(tile);
			for(var lvlIdx = 1; lvlIdx <= level; lvlIdx++)
				region = Meta(lvlIdx).Opt(region);
			return region;
		}
		/**Returns the region at the appropriate level that directly or indirectly contains this tile, if any.*/
		[CanBeNull] [MethodImpl(AggressiveInlining)] public WGRegion<T> RegionOpt(WTile tile, int level) => RegionOpt(tile.Id, level);

		/**Returns the region at the appropriate level that directly or indirectly contains this tile.*/
		public WGRegion<T> RegionReq(TID tile, int level) {
			WGRegion<T> region = Tiles.Require(tile);
			for(var lvlIdx = 1; lvlIdx <= level; lvlIdx++)
				region = Meta(lvlIdx).Require(region);
			return region;
		}
		/**Returns the region at the appropriate level that directly or indirectly contains this tile.*/
		[MethodImpl(AggressiveInlining)] public WGRegion<T> RegionReq(WTile tile, int level) => RegionReq(tile.Id, level);
		
		/**Creates a new stack containing a different kind of data.*/
		public WGLayerStack<R> ConvertData<R>(Func<WGRegion<T>, R> dataConverter) {
			var newTiles = Tiles.ConvertData(dataConverter.Invoke);
			var newStack = new WGLayerStack<R>(newTiles);
			for(var level = 1; level < Layers; level++) {
				var layer = Meta(level);
				var subLayer = Layer(level - 1);
				var newLayer = layer.ConvertData<R>(newStack, (r) => dataConverter.Invoke(r), (sub) => {
					var tile = sub.SomeTileReq();
					return newStack.RegionReq(tile, subLayer.Level);
				});
				newStack._metas.Add(newLayer);
			}
			return newStack;
		}
	}

}