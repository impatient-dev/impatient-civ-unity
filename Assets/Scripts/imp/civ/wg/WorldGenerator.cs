﻿using System;
using imp.civ.world;
using static imp.util.ImpatientUtil;

namespace imp.civ.wg {

	public static class WorldGenerator {
		public static World Generate(WGSettings settings) {
			CheckArg(settings.Width * settings.Height < 1_000_000_000, () => $"World is too large: {settings.Width} x {settings.Height}.");
			var rand = new Random(settings.Seed);
			var w = new World(settings.Width, settings.Height, settings.GridType, settings.TileArea, new Random(settings.Seed));

			WorldGenTerrain.Generate(w, settings.Terrain, rand);
			WorldGenSpawns.Generate(w, settings, rand);

			return w;
		}
	}



}