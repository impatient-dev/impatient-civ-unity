using System;
using System.Collections.Generic;
using System.Linq;
using imp.civ.world;
using imp.util;
using UnityEngine;
using static imp.util.ImpatientUtil;
using Random = System.Random;

namespace imp.civ.wg {

	public static class WorldGenTerrain {

		/**Different regions have terrain weights increased/decreased by up to this much.*/
		private const float WEIGHT_FACTOR = 0.2f;
		
		
		public static void Generate(World w, WGTerrain settings, Random rand) {
			var stack = DivideIntoRegions(w, settings, rand);
			GenerateWeights(stack, settings, rand);
			SetBorderToOceanIfDesired(w, stack, settings);
			DecideRegionTerrainTypes(w, stack, settings);
			ApplyRegionTerrainTypes(w, stack, rand);
		}

		
		private static WGLayerStack<RegionTerrain> DivideIntoRegions(World w, WGTerrain settings, Random rand) {
			var regionCount = Math.Max(1, (int) (w.TileCount / settings.TilesPerRegion));
			Debug.Log($"Generating {regionCount} regions for {w.TileCount}-tile world ({w.Width} x {w.Height}).");
			var stack = new WGLayerStack<RegionTerrain>(w, regionCount, rand, () => new RegionTerrain());
			while(true) {
				var metaRegions = (int)(stack.TopLayer.GenericRegions.Count / settings.RegionsPerRegion);
				if(metaRegions <= 2)
					return stack;
				Debug.Log($"Generating {metaRegions} level={stack.Layers} meta-regions.");
				stack.AddNextMeta(w, metaRegions, rand, () => new RegionTerrain());
			}
		}

		
		/**Applies random terrain type weights to every region at every level of the stack.*/
		private static void GenerateWeights(WGLayerStack<RegionTerrain> stack, WGTerrain settings, Random rand) {
			for(var level = 0; level < stack.Layers; level++) {
				foreach(var region in stack.Layer(level).GenericRegions) {
					foreach(var type in realTerrainTypes)
						region.Data.TypeWeights[type] = rand.NextBool() ? rand.NextFloat(1, 1 + WEIGHT_FACTOR) : rand.NextFloat(1 - WEIGHT_FACTOR, 1);
				}
			}
		}

		
		/**Changes the terrain types of regions that border the world edge, if enabled in settings.*/
		private static void SetBorderToOceanIfDesired(World w, WGLayerStack<RegionTerrain> stack, WGTerrain settings) {
			if(!settings.BorderIsOcean)
				return;
			int level = Math.Max(0, stack.Layers - 3);
			void makeOcean(int x, int y) {
				var data = stack.RegionReq(w.T.Require(x, y), level).Data;
				data.Type = TerrainType.OCEAN;
			}
			for(var x = w.X0; x <= w.X1; x++) {
				makeOcean(x, w.Y0);
				makeOcean(x, w.Y1);
			}
			for(var y = w.Y0; y <= w.Y1; y++) {
				makeOcean(w.X0, y);
				makeOcean(w.X1, y);
			}
		}


		/**Applies terrain types to the data in the lowest-level regions, respecting the weights and settings as much as possible.*/
		private static void DecideRegionTerrainTypes(World w, WGLayerStack<RegionTerrain> stack, WGTerrain settings) {
			// combine weights between different layers
			var combined = new Dictionary<WGTileRegion<RegionTerrain>, RegionTerrain>();
			foreach(var region in stack.Tiles.Regions)
				combined[region] = region.CombineData(stack, (parent, child) => child.Inside(parent));
			
			var tileCount = new Dictionary<TerrainType, int>();
			foreach(var type in realTerrainTypes)
				tileCount[type] = 0;
			
			// figure out which regions are already assigned terrain directly or indirectly
			var unassignedRegions = new List<WGTileRegion<RegionTerrain>>();
			foreach(var entry in combined) {
				if(entry.Value.Type == TerrainType.UNDECIDED) {
					unassignedRegions.Add(entry.Key);
				} else {
					entry.Key.Data.Type = entry.Value.Type;
					tileCount[entry.Value.Type] += entry.Key.Tiles.Count;
				}
			}

			// determine what fraction of the world should be each terrain type
			var expectedFractions = new Dictionary<TerrainType, float>();
			expectedFractions[TerrainType.LAND] = settings.Land / settings.TotalWeight();
			expectedFractions[TerrainType.OCEAN] = settings.Ocean / settings.TotalWeight();
			expectedFractions[TerrainType.SEA] = settings.Sea / settings.TotalWeight();
			expectedFractions[TerrainType.SWAMP] = settings.Swamp / settings.TotalWeight();

			// repeatedly assign the terrain type that is most missing to its highest-weighted region
			while(unassignedRegions.IsNotEmpty()) {
				var totalTilesAssigned = tileCount.Values.Aggregate((a, b) => a + b);
				var typeMostMissing = TerrainTypeMostMissing(totalTilesAssigned, tileCount, expectedFractions);
				var regionIdx = HighestWeightedRegionIdx(unassignedRegions, typeMostMissing, combined);
				var region = unassignedRegions[regionIdx];
				tileCount[typeMostMissing] += region.Tiles.Count;
				region.Data.Type = typeMostMissing;
				unassignedRegions.RemoveAt(regionIdx);
			}
		}

		private static TerrainType TerrainTypeMostMissing(
			int totalTilesAssigned,
			Dictionary<TerrainType, int> tilesAssigned,
			Dictionary<TerrainType, float> expectedFractions
		) {
			var ret = TerrainType.UNDECIDED;
			var retFractionOfExpected = float.PositiveInfinity;
			foreach(var type in realTerrainTypes) {
				var fraction = tilesAssigned.Require(type) / (float)totalTilesAssigned;
				var fractionOfExpected = fraction / expectedFractions.Require(type);
				if(fractionOfExpected < retFractionOfExpected) {
					ret = type;
					retFractionOfExpected = fractionOfExpected;
				}
			}
			return ret;
		}

		/**Returns an index from the regions list.*/
		private static int HighestWeightedRegionIdx(
			List<WGTileRegion<RegionTerrain>> regions,
			TerrainType type,
			Dictionary<WGTileRegion<RegionTerrain>, RegionTerrain> terrain
		) {
			CheckArg(regions.IsNotEmpty(), "No regions.");
			var retIdx = -1;
			var retWeight = float.NegativeInfinity;
			for(var r = 0; r < regions.Count; r++) {
				var region = regions[r];
				var weight = terrain.Require(region).TypeWeights.OrDefault(type, 1f);
				if(weight > retWeight) {
					retIdx = r;
					retWeight = weight;
				}
			}
			return retIdx;
		}


		/**Sets the altitude and liquid levels on all tiles based on the terrain types of the lowest-level regions.*/
		private static void ApplyRegionTerrainTypes(World w, WGLayerStack<RegionTerrain> stack, Random rand) {
			for(var x = w.X0; x <= w.X1; x++) {
				for(var y = w.Y0; y <= w.Y1; y++) {
					var tile = w.T.Require(x, y);
					var terrain = stack.Tiles.Require(tile).Data;
					switch(terrain.Type) {
						case TerrainType.LAND:
							tile.AltBase = rand.NextFloat(10, 50);
							tile.AltDiff = rand.NextFloat(100);
							break;
						case TerrainType.OCEAN:
							tile.AltBase = rand.NextFloat(-300, -150);
							tile.AltDiff = rand.NextFloat(100);
							break;
						case TerrainType.SEA:
							tile.AltBase = rand.NextFloat(-50, -20);
							tile.AltMax = rand.NextFloat(tile.AltBase, -15);
							break;
						case TerrainType.SWAMP:
							tile.AltBase = rand.NextFloat(-2f, -0.1f);
							tile.AltMax = rand.NextFloat(tile.AltBase, 5);
							break;
						default:
							UnexpectedType<TerrainType>(terrain.Type);
							break;
					}
					tile.LiquidDepth = Mathf.Max(0, -tile.AltBase);
				}
			}
		}
		

		private class RegionTerrain {
			public TerrainType Type;
			/**Weights that make different types of terrain more or less likely in this region compared with others. Weights are multiplicative.
			 * If a type isn't in the map, its weight is 1. UNDECIDED should not have a weight.*/
			public readonly Dictionary<TerrainType, float> TypeWeights;

			public RegionTerrain(TerrainType type, Dictionary<TerrainType, float> typeWeights) {
				Type = type;
				TypeWeights = typeWeights;
			}

			public RegionTerrain() : this(TerrainType.UNDECIDED, new Dictionary<TerrainType, float>()) {}

			/**Combines the weights etc with the corresponding values from the parent region that contains this one.*/
			public RegionTerrain Inside(RegionTerrain parent) {
				// child terrain type takes precedence
				var t = parent.Type;
				if(Type != TerrainType.UNDECIDED)
					t = Type;
				
				// multiply weights, omitting 1.0
				var d = new Dictionary<TerrainType, float>(realTerrainTypes.Length);
				foreach(var type in realTerrainTypes) {
					var weight = TypeWeights.OrDefault(type, 1f) * parent.TypeWeights.OrDefault(type, 1f);
					if(weight != 1f) // 1 can be represented exactly by a float
						d[type] = weight;
				}

				return new RegionTerrain(t, d);
			}
		}

		
		private enum TerrainType {
			UNDECIDED,
			LAND,
			OCEAN,
			SEA,
			SWAMP,
		}
		private static readonly TerrainType[] realTerrainTypes = {
			TerrainType.LAND,
			TerrainType.OCEAN,
			TerrainType.SEA,
			TerrainType.SWAMP,
		};
	}

}