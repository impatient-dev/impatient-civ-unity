using System;
using System.Collections.Generic;
using System.Linq;
using imp.civ.change;
using imp.civ.util;
using imp.civ.world;
using imp.civ.world.land;
using UnityEngine;
using static imp.util.Continue;
using static imp.util.ImpatientUtil;
using Random = System.Random;

namespace imp.civ.wg {
	public static class WorldGenSpawns {
		/**Randomly places spawn portals.*/
		public static void Generate(World w, WGSettings settings, Random rand) {
			CheckArg(settings.SpawnPortals >= 0, () => $"{settings.SpawnPortals} spawn portals");
			for(int p = 0; p < settings.SpawnPortals; p++) {
				var tile = ChoosePortalLocation(w, rand);
				ChWorld.LandCreateFromEmpty(w, tile, new SpawnPortal(), SpawnPortal.LAND);
			}
		}

		private static WTile ChoosePortalLocation(World w, Random rand) {
			// find some candidate locations
			var tiles = new List<RatedTile>(10);
			for(int c = 0; c < 50; c++) {
				var tile = rand.NextTile(w);
				var rating = SuitabilityForPortal(w, tile);
				if(rating >= 0) {
					tiles.Add(new RatedTile(tile, rating));
					if(tiles.AtCapacity())
						break;
				}
			}

			if(tiles.Count == 0)
				throw new Exception("Failed to find enough places to put spawn portals.");
			return (
				from t in tiles
				orderby t.Rating descending
				select t.WTile
			).First();
		}


		/**0 is bad, 1 is good, negative numbers are unacceptable.*/
		private static float SuitabilityForPortal(World w, WTile tile) {
			if(tile.LiquidDepth > 0) return -1;
			
			// ensure no portals nearby
			var spawnPortalConflict = false;
			TileSearch.Search(w, tile, 5, t => {
				if(t.AnyPlot(p => p.Use is SpawnPortal))
					spawnPortalConflict = true;
				return StopIf(spawnPortalConflict);
			});
			if(spawnPortalConflict)
				return -1;

			// apply penalty for being on a tiny island or otherwise not around much land
			var nearby = 0;
			TileSearch.Search(w, tile, 3, t => { nearby++; return CONTINUE; });
			var connectedLandNearby = 0;
			TileSearch.Search(w, tile, 3, t => {
				if(t.LiquidDepth == 0)
					connectedLandNearby++;
				return StopIf(t.LiquidDepth > 0);
			});
			var minFractionForNoPenalty = 0.6f;
			return Mathf.Min(1, connectedLandNearby / (nearby * 0.6f));
		}


		private struct RatedTile {
			public readonly WTile WTile;
			public readonly float Rating;

			public RatedTile(WTile tile, float rating) {
				WTile = tile;
				Rating = rating;
			}
		}
	}
}