using System.Collections.Generic;
using imp.util;
using JetBrains.Annotations;

namespace imp.civ.world {

	/**Stores all vertices for a world. The constructor creates all vertices.*/
	public class WVertices {
		private readonly Dictionary<VID, TVertex> ById = new Dictionary<VID, TVertex>();
		
		public WVertices(World w) {
			for(int x = 0; x < w.Width; x++)
				for(int y = 0; y < w.Height; y++)
					foreach(var id in w.G.VerticesAround(new TID(x, y)))
						ById.GetOrCreate(id, () => new TVertex(id));
		}

		public TVertex Require(VID id) => ById.Require(id);
		[CanBeNull] public TVertex Opt(VID id) => ById.Opt(id);
	}
}