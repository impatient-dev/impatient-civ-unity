using System.Collections.Generic;
using imp.util;
using JetBrains.Annotations;

namespace imp.civ.world {

	/**Stores all sides for a world. The constructor creates all sides.*/
	public class WSides {
		private readonly Dictionary<SID, WSide> ById = new Dictionary<SID, WSide>();
		
		public WSides(World w) {
			for(int x = 0; x < w.Width; x++)
				for(int y = 0; y < w.Height; y++)
					foreach(var sid in w.G.SidesAround(new TID(x, y)))
						ById.PutNew(sid, new WSide(sid));
		}

		public WSide Require(SID id) => ById.Require(id);
		[CanBeNull] public WSide Opt(SID id) => ById.Opt(id);
	}
}