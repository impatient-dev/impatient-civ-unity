﻿using UnityEngine;
using static imp.util.ImpatientUnityUtil;

namespace imp.civ.world {

	/**The simplest kind of grid, composed of squares.
	 *
	 * (0,0) in cellishes is the lower-left vertex of tile (0,0).
	 * Vertex (X,Y) is southwest of tile (X,Y).
	 * Edge (X,Y,?) is south/west of tile (X,Y).*/
	public class SquareGrid : GridType {
		public int N => 4;
		public int EPV => 4;
		public float EdgeLength => 1f;
		public float InternalAngleDeg => 90f;

		public (VID, VID) VerticesOfEdge(EID edge) {
			var (x, y, south) = (edge.X, edge.Y, edge.Q == 1);
			return (new VID(x, y), south ? new VID(x + 1, y) : new VID(x, y + 1));
		}

		public TID[] TilesAround(TID tile) => new[] {
			new TID(tile.X + 1, tile.Y),
			new TID(tile.X, tile.Y + 1),
			new TID(tile.X - 1, tile.Y),
			new TID(tile.X, tile.Y - 1),
		};
		public EID[] EdgesAround(TID tile) => new[] {
			new EID(tile.X + 1, tile.Y, 0),
			new EID(tile.X, tile.Y + 1, 1),
			new EID(tile.X, tile.Y, 0),
			new EID(tile.X, tile.Y, 1),
		};
		public VID[] VerticesAround(TID tile) => new[] {
			new VID(tile.X + 1, tile.Y + 1),
			new VID(tile.X, tile.Y + 1),
			new VID(tile.X, tile.Y),
			new VID(tile.X + 1, tile.Y),
		};
		public SID[] SidesAround(TID tile) => new[] {
			new SID(new EID(tile.X + 1, tile.Y, 0), false, tile),
			new SID(new EID(tile.X, tile.Y + 1, 1), false, tile),
			new SID(new EID(tile.X, tile.Y, 0), true, tile),
			new SID(new EID(tile.X, tile.Y, 1), true, tile),
		};

		public Vector2 Center(TID tile) => new Vector2(tile.X + 0.5f, tile.Y + 0.5f);
		public Vector2 VertexPos(VID vertex) => V2(vertex.X, vertex.Y);

		public TID TileContaining(float x, float y) => new TID((int)Mathf.Floor(x), (int)Mathf.Floor(y));

		public int[] FillTriangleIndices { get; } = { 0, 3, 2, 2, 1, 0 };
	}
}