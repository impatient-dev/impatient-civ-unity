using System;
using JetBrains.Annotations;

namespace imp.civ.world {
	/**Stores Tiles for a World. The constructor creates all Tiles.*/
	public class WTiles {
		private readonly WTile[] _array;
		private readonly int Width, Height;

		public WTiles(int width, int height) {
			Width = width;
			Height = height;
			_array = new WTile[Width * Height];
			for(var x = 0; x < Width; x++)
				for(var y = 0; y < Height; y++)
					_array[Idx(x, y)] = new WTile(new TID(x, y));
		}

		private int Idx(TID id) => Idx(id.X, id.Y);
		private int Idx(int x, int y) {
			if (x < 0 || x >= Width || y < 0 || y >= Height)
				throw new Exception($"Invalid tile ({x},{y}) in {Width}x{Height} world");
			return y * Width + x;
		}

		public WTile Require(TID id) => _array[Idx(id)];
		public WTile Require(int x, int y) => Require(new TID(x, y));

		/**Tolerates out-of-bound indexes.*/
		[CanBeNull] public WTile Opt(TID id) {
			if (id.X < 0 || id.Y < 0 || id.X >= Width || id.Y >= Height)
				return null;
			return _array[Idx(id.X, id.Y)];
		}
	}
}