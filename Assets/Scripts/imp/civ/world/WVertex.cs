namespace imp.civ.world {
	public class TVertex {
		public readonly VID Id;

		public TVertex(VID id) {
			Id = id;
		}
	}
}