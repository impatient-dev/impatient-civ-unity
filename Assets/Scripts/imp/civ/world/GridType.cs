﻿using UnityEngine;

namespace imp.civ.world {
	/**Defines the relationships between tiles, edges, vertices, and sides.
	 *
	 * Functions that find all tiles/etc. next to a tile/etc. find all that could possibly exist.
	 * They do not check that the returned coordinates are within the boundaries of a world.
	 * 
	 * Functions that return arrays normally return things in counter-clockwise order.
	 * This works well since unity culls triangles that aren't in clockwise order, and the world is viewed from the bottom (-Z).*/
	public interface GridType {
		/**How many edges (or vertices) each tile touches. Tiles are (this)-sided polygons.*/
		int N { get; }
		/**How many edges share each vertex. (Edges per vertex.)*/
		int EPV { get; }
		/**How long each edge is.*/
		float EdgeLength { get; }
		float InternalAngleDeg { get; }

		/**order: counter-clockwise from east/northeast.*/
		TID[] TilesAround(TID tile);
		/**order: counter-clockwise from east/northeast.*/
		EID[] EdgesAround(TID tile);
		/**order: counter-clockwise from east/northeast.*/
		VID[] VerticesAround(TID tile);
		/**order: same as for edges: counter-clockwise from east/northeast.*/
		SID[] SidesAround(TID tile);
		/**Order: undefined.*/
		(VID, VID) VerticesOfEdge(EID edge);

		Vector2 Center(TID tile);
		Vector2 VertexPos(VID vertex);

		/**In order to fill the interior of a tile, get the vertices and make triangles by using these to index into the array of vertices.*/
		int[] FillTriangleIndices { get; }

		/**Returns the identifying coordinates of the tile that contains a position.*/
		TID TileContaining(float x, float y);
	}


	static class GridTypes {
		public static readonly GridType Square = new SquareGrid();
	}


	static class GridTypeExtensions {
		/**Given an edge and a vertex, returns the other vertex of that edge.*/
		public static VID OtherVertex(this GridType g, EID edge, VID vertex) {
			var (a, b) = g.VerticesOfEdge(edge);
			return vertex == a ? b : a;
		}
	}


	/**Tile ID. (0,0) is the bottom-left of the world. X increases to the right, and Y increases up.*/
	public struct TID {
		public readonly int X, Y;

		public TID(int x, int y) {
			X = x;
			Y = y;
		}
		
		public static bool operator ==(TID a, TID b)  => a.X == b.X && a.Y == b.Y;
		public static bool operator !=(TID a, TID b) => !(a == b);
		public override bool Equals(object obj) => obj is TID that && this == that;
		public override int GetHashCode() => (X, Y).GetHashCode();
		public override string ToString() => $"TID({X}, {Y})";
		public WTile In(World w) => w.T.Require(this);
	}
	

	/**Vertex ID.*/
	public struct VID {
		/**Square grid: the coordinates of the tile to northeast.*/
		public readonly int X, Y;

		public VID(int x, int y) {
			X = x;
			Y = y;
		}

		public static bool operator ==(VID a, VID b)  => a.X == b.X && a.Y == b.Y;
		public static bool operator !=(VID a, VID b) => !(a == b);
		public override bool Equals(object obj) => obj is VID that && this == that;
		public override int GetHashCode() => (X, Y).GetHashCode();
		public override string ToString() => $"VID({X}, {Y})";
		// public WVertex In(World w) => w.V.Require(this);
	}


	/**Edge ID.*/
	public struct EID {
		/**Identifiers for the tile to the south/west of this edge.*/
		public readonly int X, Y;
		/**Square grid: 0 if this edge is vertical and west of the tile; 1 if it's horizontal and south of the tile.*/
		public readonly byte Q;

		public EID(int x, int y, byte q) {
			X = x;
			Y = y;
			Q = q;
		}

		public static bool operator ==(EID a, EID b) => a.X == b.X && a.Y == b.Y && a.Q == b.Q;
		public static bool operator !=(EID a, EID b) => !(a == b);
		public override bool Equals(object obj) => obj is EID that && this == that;
		public override int GetHashCode() => (X, Y, Q).GetHashCode();
		public override string ToString() => $"EID({X}, {Y} Q={Q})";
		// public WEdge In(World w) => w.E.Require(this);
	}


	/**Side ID.*/
	public struct SID {
		public readonly EID Edge;
		/**Whether this is the north/east (upper/right) side of the edge.
		 * For edges oriented SW-NE, the left side is west (instead of north) and the right is east (instead of south).*/
		public readonly bool NE;
		public readonly TID Tile;

		public SID(EID edge, bool ne, TID tile) {
			Edge = edge;
			NE = ne;
			Tile = tile;
		}
		
		public static bool operator ==(SID a, SID b) => a.Edge == b.Edge && a.NE == b.NE;
		public static bool operator !=(SID a, SID b) => !(a == b);
		public override bool Equals(object obj) => obj is SID that && this == that;
		public override int GetHashCode() => (Edge, NE).GetHashCode();
		public override string ToString() => "SID(" + Edge + ' ' + (NE ? "NE": "SW") + ' ' + Tile + ')';
		// public WSide In(World w) => w.S.Require(this);
	}
}