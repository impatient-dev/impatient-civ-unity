using System;
using System.Collections.Generic;
using imp.civ.world.land;
using UnityEngine;
using static imp.civ.world.IdUtils;
using static imp.util.ImpatientUtil;

namespace imp.civ.world {
	public class WTile {
		public TID Id;
		public override string ToString() => $"Tile({Id})";

		public WTile(TID id) {
			Id = id;
			var plotId =  new PlotId(1);
			Plots[plotId] = new LandPlot(plotId, Land.TILE, new EmptyLand());
		}
		
		
		
		// GRID

		public Vector2 Center(World w) => w.G.Center(Id);

		public void ForTilesAround(World w, Action<WTile> action) {
			foreach(var id in w.G.TilesAround(Id)) {
				var tile = w.T.Opt(id);
				if(tile != null)
					action.Invoke(tile);
			}
		}

		public List<WTile> ListTilesAround(World w) => w.G.TilesAround(Id).MapOpt(id => w.T.Opt(id));

		public void ForEdgesAround(World w, Action<TEdge> action) {
			foreach(var id in w.G.EdgesAround(Id)) {
				var edge = w.E.Opt(id);
				if(edge != null)
					action.Invoke(edge);
			}
		}

		public List<TEdge> ListEdgesAround(World w) => w.G.EdgesAround(Id).MapOpt(id => w.E.Opt(id));

		public void ForSides(World w, Action<WSide> action) {
			foreach(var id in w.G.SidesAround(Id))
				action.Invoke(w.S.Require(id));
		}

		public WSide[] ListSides(World w) => w.G.SidesAround(Id).Map(id => w.S.Require(id));

		public void ForVertices(World w, Action<TVertex> action) {
			foreach(var id in w.G.VerticesAround(Id)) {
				var vertex = w.V.Opt(id);
				if(vertex != null)
					action.Invoke(vertex);
			}
		}

		public TVertex[] ListVertices(World w) => w.G.VerticesAround(Id).Map(id => w.V.Require(id));
		
		
		
		// TERRAIN

		/**The highest point of land.*/
		public float AltMax = 0f;
		/**The altitude where most of the land is. This is where buildings, people, etc. are usually assumed to be.
		 * This is the minimum altitude in the tile as well.*/
		public float AltBase = 0f;

		/**Max minus min altitude of the land. Determines how hilly/mountainous this tile is.*/
		public float AltDiff {
			get => AltMax - AltBase;
			set {
				CheckArg(value >= 0, () => $"Negative alt diff: {value}.");
				AltMax = AltBase + value;
			}
		}

		private float _liquidDepth = 0f;
		/**This tile is full of liquid to the specified depth (if flooded, e.g. lake or sea) - nonnegative.
		 * The liquid is assumed to be water for now.*/
		public float LiquidDepth {
			get => _liquidDepth;
			set {
				CheckArg(value >= 0, () => $"Negative liquid depth: {value}.");
				_liquidDepth = value;
			}
		}
		/**This tile is full of liquid up to the specified altitude (if flooded, e.g. lake or sea).
		 * This is never lower than the minimum tile altitude.*/
		public float LiquidAlt {
			get => AltBase + _liquidDepth;
			set => LiquidDepth = value - AltBase;
		}
		
		
		
		// LAND USE
		
		/**All plots of land in this tile. The land used always adds up to Land.TILE.*/
		public readonly Dictionary<PlotId, LandPlot> Plots = new Dictionary<PlotId, LandPlot>();

		public LandPlot AddPlot(World w, Land amount, LandUse use) {
			var id = RandPlotId(Plots, w.R);
			var plot = new LandPlot(RandPlotId(Plots, w.R), amount, use);
			Plots.PutNew(id, plot);
			return plot;
		}

		public void RemovePlot(LandPlot plot) => Plots.Remove(plot.Id);

		public bool AnyPlot(Predicate<LandPlot> func) {
			foreach(var plot in Plots.Values)
				if(func.Invoke(plot))
					return true;
			return false;
		}
	}
}