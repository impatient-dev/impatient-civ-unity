using System;
using System.Collections.Generic;
using imp.util;

namespace imp.civ.world {

	public static class IdUtils {
		private static readonly int TRIES = 8;
		
		/**Returns a random ID not currently in the map.*/
		public static PlotId RandPlotId<V>(Dictionary<PlotId, V> map, Random rand) {
			for(int c = 0; c < TRIES; c++) {
				var i = rand.Next(ushort.MaxValue - 1) + 1;
				var id = new PlotId((ushort)i);
				if(!map.ContainsKey(id)) return id;
			}
			throw new Exception("Failed to generate plot ID");
		}
	}
	
	
	

	/**Uniquely identifies a plot of land within a single tile.*/
	public readonly struct PlotId {
		public readonly ushort Value;

		public PlotId(ushort s) {
			Value = s;
		}

		public static bool operator ==(PlotId a, PlotId b) => a.Value == b.Value;
		public static bool operator !=(PlotId a, PlotId b) => !(a == b);
		public override bool Equals(object obj) => obj is PlotId that && this == that;
		public override int GetHashCode() => Value.GetHashCode();
		public override string ToString() => $"PlotId({Value.ToHexPad()})";
	}

	/**Uniquely identifies a plot of land within a world.*/
	public readonly struct PlotRef {
		public readonly TID Tile;
		public readonly PlotId Plot;

		public PlotRef(TID tile, PlotId plot) {
			Tile = tile;
			Plot = plot;
		}
		
		public static bool operator ==(PlotRef a, PlotRef b) => a.Tile == b.Tile && a.Plot == b.Plot;
		public static bool operator !=(PlotRef a, PlotRef b) => !(a == b);
		public override bool Equals(object obj) => obj is PlotRef that && this == that;
		public override int GetHashCode() => (Tile, Plot).GetHashCode();
		public override string ToString() => $"PlotRef({Tile}{Plot})";
	}
}