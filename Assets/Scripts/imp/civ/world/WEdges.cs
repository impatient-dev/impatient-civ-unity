using System.Collections.Generic;
using imp.util;
using JetBrains.Annotations;

namespace imp.civ.world {
	/**Stores edges for a world. The constructor creates all edges.*/
	public class WEdges {
		private readonly Dictionary<EID, TEdge> ById = new Dictionary<EID, TEdge>();

		public WEdges(World w) {
			for(int x = 0; x < w.Width; x++)
				for(int y = 0; y < w.Height; y++)
					foreach(var id in w.G.EdgesAround(new TID(x, y)))
						ById.GetOrCreate(id, () => new TEdge(id));
		}


		public TEdge Require(EID id) => ById.Require(id);
		[CanBeNull] public TEdge Opt(EID id) => ById.Opt(id);
	}
}