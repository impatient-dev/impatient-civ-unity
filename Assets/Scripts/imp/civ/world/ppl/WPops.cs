namespace imp.civ.world.ppl {


	public struct PopId {
		private readonly uint _val;

		public PopId(uint val) {
			_val = val;
		}

		public override int GetHashCode() => _val.GetHashCode();
	}
	
	
	public class WPops {
		
	}
}