using static imp.util.ImpatientUtil;

namespace imp.civ.world.ppl {
	
	/**A group of people with similar traits.*/
	public class Pop {
		public readonly PopId Id;
		
		private int _count;
		/**The number of people in this pop. Always greater than zero.*/
		public int Count {
			get => _count;
			set {
				CheckArg(value > 0, () => $"Invalid pop count: {value}");
				_count = value;
			}
		}
		
		public Pop(PopId id, int count) {
			Id = id;
			Count = count;
		}
	}
	
}