﻿using System;

namespace imp.civ.world {

	/**A virtual world full of cells, edges, etc.*/
	public class World {
		public readonly int Width, Height;
		public readonly GridType G;
		
		public int TileCount => Width * Height;
		/**Minimum X coordinate.*/
		public int X0 => 0;
		/**Minimum Y coordinate.*/
		public int Y0 => 0;
		/**Maximum X coordinate.*/
		public int X1 => Width - 1;
		/**Maximum Y coordinate.*/
		public int Y1 => Height - 1;

		public readonly WorldConfig C;
		public readonly WTiles T;
		public readonly WEdges E;
		public readonly WSides S;
		public readonly WVertices V;
		/**Random number generator, for generating IDs and resolving probabilities.
		 * This is part of the state of the world; never request random numbers from this during the planning phase, only during the sim phase.*/
		public readonly Random R;

		/**For tile area, see WorldConfig.*/
		public World(int width, int height, GridType gridType, float tileArea, Random r) {
			Width = width;
			Height = height;
			G = gridType;
			C = new WorldConfig(G, tileArea);
			T = new WTiles(Width, Height);
			E = new WEdges(this);
			S = new WSides(this);
			V = new WVertices(this);
			R = r;
		}
	}

}