namespace imp.civ.world {
	/**An edge - a line separating 2 tiles. (The name "Edge" was already taken.)*/
	public class TEdge {
		/**Identifier understood by the grid type.*/
		public readonly EID Id;

		public TEdge(EID id) {
			Id = id;
		}
	}
}