using System;

namespace imp.civ.world.land {
	
	/**An amount of land. Measured with some kind of non-floating-point number, so things add up precisely.
	 * Amounts of Land can only be compared within a single world.*/
	public struct Land {
		/**How much land is present in a tile.*/
		private static readonly long TILE_AMT = 10_000_000;
		public static readonly Land ZERO = new Land(0);
		/**A whole tile worth of land.*/
		public static readonly Land TILE = new Land(TILE_AMT);
		
		private readonly long _val;

		private Land(long val) {
			_val = val;
		}

		public bool Positive => _val > 0;

		/**Returns how many whole tiles this land is equivalent to.*/
		public float Tiles => _val / (float)TILE_AMT;
		/**Returns how many square meters this land is equivalent to.*/
		public float Sqm(World w) => Tiles * w.C.Area;

		/**Converts a float to an amount of land, where 1.0f is the amount of land in a tile.*/
		public static Land OfTiles(float tiles) {
			var amt = (long)Math.Round(tiles * TILE_AMT);
			if(amt == 0 && tiles > 0)
				amt = 1;
			return new Land(amt);
		}

		/**Converts square meters to land.*/
		public static Land OfSqm(float sqm, World w) => OfTiles(sqm / w.C.Area);

		public override string ToString() => $"{Tiles}";

		public static bool operator ==(Land a, Land b) => a._val == b._val;
		public static bool operator !=(Land a, Land b) => a._val != b._val;
		public static bool operator <(Land a, Land b) => a._val < b._val;
		public static bool operator >(Land a, Land b) => a._val > b._val;
		public static bool operator <=(Land a, Land b) => a._val <= b._val;
		public static bool operator >=(Land a, Land b) => a._val >= b._val;

		public static Land operator +(Land a, Land b) => new Land(a._val + b._val);
		public static Land operator -(Land a, Land b) => new Land(a._val - b._val);
		public static Land operator *(int i, Land land) => new Land(i * land._val);
		public static Land operator *(Land land, int i) => new Land(i * land._val);

		public static Land operator /(Land land, int i) {
			var ret = land._val / i;
			if(ret == 0 && land._val > 0)
				ret = 1;
			return new Land(ret);
		}

		public override bool Equals(Object obj) => obj is Land that && this == that;
		public override int GetHashCode() => _val.GetHashCode();
	}


	/**A thing that can constitute a plot of land.
	 * This interface will get methods eventually.*/
	public interface LandUse {
	}


	/**An amount of land in a tile, dedicated to a specific use.*/
	public class LandPlot {
		public readonly PlotId Id;
		public Land Land;
		public LandUse Use;

		public LandPlot(PlotId id, Land land, LandUse use) {
			Id = id;
			Land = land;
			Use = use;
		}
	}
}