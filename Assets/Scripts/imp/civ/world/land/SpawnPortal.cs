namespace imp.civ.world.land {
	public class SpawnPortal : LandUse {
		/**How much land a portal requires.*/
		public static readonly Land LAND = Land.TILE / 2;
	}
}