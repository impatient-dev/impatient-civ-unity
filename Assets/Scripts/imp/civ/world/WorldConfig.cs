using UnityEngine;
using static imp.util.ImpatientUtil;

namespace imp.civ.world {

	/**Geometry settings for the world.*/
	public class WorldConfig {
		/**The number of square meters in a tile. Adjusting this adjusts how much can fit in each tile.*/
		public readonly float Area;
		/**How many meters long each edge is. This is determined by Area and the grid type.
		This determines how hard it is to build something (e.g. a wall) that spans the length of an edge.*/
		public readonly float Elen;
		/**How many meters deep each side is. This is determined by Area and the grid type.
		 * This determines how hard it is to build something (e.g. a road) that connects the center of a tile to an edge.*/
		public readonly float Slen;


		public WorldConfig(GridType g, float area) {
			CheckArg(area > 0, () => $"Invalid area: {area}");
			Area = area;
			if(g == GridTypes.Square) {
				Elen = Mathf.Sqrt(Area);
				Slen = Elen;
			} else UnexpectedType<GridType>(g);
		}
	}
}