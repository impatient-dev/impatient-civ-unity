using UnityEngine;

namespace imp.civ.world {
	public class WSide {
		public readonly SID Id;

		public WSide(SID id) {
			Id = id;
		}

		public WTile Tile(World w) => w.T.Require(Id.Tile);
		public TEdge Edge(World w) => w.E.Require(Id.Edge);

		/**The direction from the center of the tile to the center of the edge, as a non-unit vector.*/
		public Vector2 BearingVec(World w) {
			var (va, vb) = w.G.VerticesOfEdge(Id.Edge);
			var edgeMidpoint = (w.G.VertexPos(va) + w.G.VertexPos(vb)) / 2;
			return edgeMidpoint - w.G.Center(Id.Tile);
		}
	}
}