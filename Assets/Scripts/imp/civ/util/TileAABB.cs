using System;
using imp.civ.world;

namespace imp.civ.util {

	/**A rectangular region of tiles in a world; an axis-aligned bounding box.
	 * The lower-left of the region is (X0, Y0); the upper-right is (X1, Y1). The bounds are inclusive.*/
	public readonly struct TileAABB {
		public readonly int X0, Y0, X1, Y1;

		private TileAABB(int x0, int y0, int x1, int y1) {
			X0 = x0;
			Y0 = y0;
			X1 = x1;
			Y1 = y1;
		}

		/**Removes any parts of this AABB that are outside of the world.*/
		public TileAABB Clamp(World w) => new TileAABB(Math.Max(0, X0), Math.Max(0, Y0), Math.Min(w.X1, X1), Math.Min(w.Y1, Y1));

		public bool Contains(TID tile) => tile.X >= X0 && tile.Y >= Y0 && tile.X <= X1 && tile.Y <= Y1;

		public void ForEachTile(Action<TID> action) {
			for(int x = X0; x <= X1; x++)
				for(int y = Y0; y <= Y1; y++)
					action.Invoke(new TID(x, y));
		}
		public void ForEachTile(World w, Action<WTile> action) {
			for(int x = X0; x <= X1; x++)
				for(int y = Y0; y <= Y1; y++)
					action.Invoke(w.T.Require(x, y));
		}

		/**Generates an AABB that covers the whole world.*/
		public static TileAABB World(World w) => new TileAABB(0, 0, w.X1, w.Y1);

		/**Generates a square AABB that is centered on a particular tile and extends horizontally and vertically the provided number of tiles.
		 * The AABB will contain (2 * radiusX + 1) columns and (2 * radiusY + 1) rows.*/
		public static TileAABB Centered(TID center, int radiusX, int radiusY) =>
			new TileAABB(center.X - radiusX, center.Y - radiusY, center.X + radiusX, center.Y + radiusY);
	}
}