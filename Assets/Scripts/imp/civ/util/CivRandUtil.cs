using System;
using imp.civ.world;

namespace imp.civ.util {
	public static class CivRandUtil {
		public static WTile NextTile(this Random rand, World w) => w.T.Require(rand.Next(w.X1), rand.Next(w.Y1));
	}
}