using System;
using imp.civ.world;
using static imp.util.ImpatientUtil;

namespace imp.civ.util {
	
	/**Stores an item per grid type. Immutable.*/
	public class PerGridType<T> {
		private readonly T Square;

		/**Constructor that takes an item for each grid type.*/
		public PerGridType(T square) {
			Square = square;
		}

		/**Constructor that computes an item for each grid type by supplying each type to a function.*/
		public PerGridType(Func<GridType, T> function) : this(function.Invoke(GridTypes.Square)) {}

		public T Req(GridType g) {
			if(g == GridTypes.Square) return Square;
			return UnexpectedType<T>(g);
		}
	}
}