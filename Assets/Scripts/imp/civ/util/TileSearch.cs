using System;
using System.Collections.Generic;
using imp.civ.world;
using imp.util;

namespace imp.civ.util {
	public static class TileSearch {
		/**Finds all tiles up to $distance from the start, by recursively visiting all neighbors. For each unique tile found, calls the function. (Excluding the start tile.)
		 * If the function returns stop, we will not continue the search from this tile (it will be treated as if this tile has no neighbors),
		 * but we will continue searching from any other tiles where continue was returned.*/
		public static void Search(World w, WTile start, int distance, Func<WTile, Continue> func) {
			var visited = new HashSet<TID>();

			void recursive(WTile t, int d) {
				if(d > distance)
					return;
				t.ForTilesAround(w, neighbor => {
					if(visited.Add(neighbor.Id) && func.Invoke(neighbor).Go)
						recursive(neighbor, d + 1);
				});
			}
			
			recursive(start, 1);
		}
	}
}